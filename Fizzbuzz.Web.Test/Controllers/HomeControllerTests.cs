﻿using Fizzbuzz.BusinessLogic.Classes;
using Fizzbuzz.BusinessLogic.Interfaces;
using Fizzbuzz.Web.Controllers;
using Fizzbuzz.Web.Models;
using NUnit.Framework;
using System.Web.Mvc;
using System.ComponentModel;
using PagedList;
using Moq;
using System;
using System.Collections.Generic;

namespace Fizzbuzz.Web.Test
{
    public class HomeControllerTests
    {
        private Mock<IFizzbuzzLogic> _fizzbuzzLogic;
        private Mock<IDayTimeProvider> _dayTimeProvider;

        [SetUp]
        public void Setup()
        {
            _fizzbuzzLogic = new Mock<IFizzbuzzLogic>();
            _fizzbuzzLogic.Setup(s => s.GetFizzBuzzList(It.IsAny<int>(), It.IsAny<DayOfWeek>())).Returns(new List<string>());

            _dayTimeProvider = new Mock<IDayTimeProvider>();
        }

        [Test]
        public void Result_FizzbuzzModelWithInputStringTenAndPageNumberOne_ReturnViewResultWithInputStringAndOutputList()
        {
            // Arrange
            var HomeController = new HomeController(_fizzbuzzLogic.Object, _dayTimeProvider.Object);
            var DummyModel = new FizzbuzzModel
            {
                inputString = "10"
            };
            var page = 1;

            // Act
            var ViewResult = HomeController.Result(DummyModel, page) as ViewResult;
            var FizzbuzzModel = ViewResult.Model as FizzbuzzModel;

            // Assert
            string ExpectedInputString = "10";

            Assert.AreEqual(ExpectedInputString, FizzbuzzModel.inputString);
            Assert.IsAssignableFrom<PagedList<string>>(FizzbuzzModel.outputList);

        }

        [Test]
        public void Result_InputStringTenAndPageNumberOne_ReturnViewResultWithInputStringAndOutputList()
        {
            // Arrange
            var HomeController = new HomeController(_fizzbuzzLogic.Object, _dayTimeProvider.Object);
            var inputString = "10";
            var page = 1;

            // Act
            var ViewResult = HomeController.Result(inputString, page) as ViewResult;
            var FizzbuzzModel = ViewResult.Model as FizzbuzzModel;

            // Assert
            string ExpectedInputString = "10";

            Assert.AreEqual(ExpectedInputString, FizzbuzzModel.inputString);
            Assert.IsAssignableFrom<PagedList<string>>(FizzbuzzModel.outputList);
        }
    }
}
