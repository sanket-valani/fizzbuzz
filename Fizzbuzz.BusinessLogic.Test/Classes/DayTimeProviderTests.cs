﻿using System;
using System.Collections.Generic;
using System.Text;
using Fizzbuzz.BusinessLogic.Classes;
using NUnit.Framework;

namespace Fizzbuzz.BusinessLogic.Test
{
    class DayTimeProviderTests
    {

        [Test]
        public void GetDay_CheckForCurrentDay_ReturnsCurrentDay()
        {
            // Arrange
            var DayTimeProvider = new DayTimeProvider();

            // Act
            var Result = DayTimeProvider.GetDay();

            // Assert
            DayOfWeek Expected = DateTime.Today.DayOfWeek;
            Assert.AreEqual(Expected, Result);
        }
    }
}
