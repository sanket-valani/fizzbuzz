﻿using System;
using System.Collections.Generic;
using System.Text;
using Fizzbuzz.BusinessLogic.Classes;
using Fizzbuzz.BusinessLogic.Interfaces;
using NUnit.Framework;
using Moq;

namespace Fizzbuzz.BusinessLogic.Test
{
    class FizzbuzzLogicTests
    {
        private IEnumerable<IRule> _rules;
        private Mock<IDayTimeProvider> _dayTimeProvider;

        [SetUp]
        public void Setup()
        {
            _rules = new List<IRule>() { new Mock<IRule>().Object, new Mock<IRule>().Object };
            _dayTimeProvider = new Mock<IDayTimeProvider>();
        }

        [Test]
        public void GetFizzBuzzList_QueryNumberIsTen_ReturnsListOfStringOfSizeTen()
        {
            // Arrange
            var FizzbuzzLogic = new FizzbuzzLogic( _rules , _dayTimeProvider.Object);

            // Act
            var Result = FizzbuzzLogic.GetFizzBuzzList(10, DayOfWeek.Monday);

            // Assert
            Assert.AreEqual(10, Result.Count);
            Assert.IsAssignableFrom<List<string>>(Result);
        }

        [Test]
        public void GetFizzBuzzList_QueryNumberIsFifteen_ReturnsListOfStringOfSizeFifteen()
        {
            // Arrange
            var FizzbuzzLogic = new FizzbuzzLogic( _rules, _dayTimeProvider.Object);

            // Act
            var Result = FizzbuzzLogic.GetFizzBuzzList(15, DayOfWeek.Monday);

            // Assert
            Assert.AreEqual(15, Result.Count);
            Assert.IsAssignableFrom<List<string>>(Result);
        }

    }
}
