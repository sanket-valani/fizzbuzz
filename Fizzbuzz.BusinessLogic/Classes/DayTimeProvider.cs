﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Fizzbuzz.BusinessLogic.Interfaces;

namespace Fizzbuzz.BusinessLogic.Classes
{
    public class DayTimeProvider : IDayTimeProvider
    {
        public DayOfWeek GetDay()
        {
            return DateTime.Today.DayOfWeek;
        }
    }
}
