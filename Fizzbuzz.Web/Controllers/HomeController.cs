﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using PagedList.Mvc;
using Fizzbuzz.BusinessLogic.Interfaces;
using Fizzbuzz.Web.Models;

namespace Fizzbuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFizzbuzzLogic _fizzbuzzLogic = null;
        private readonly IDayTimeProvider _dayTimeProvider = null;
        public HomeController(IFizzbuzzLogic fizzbuzzLogic, IDayTimeProvider dayTimeProvider)
        {
            _fizzbuzzLogic = fizzbuzzLogic;
            _dayTimeProvider = dayTimeProvider;
        }
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Result(FizzbuzzModel fizzbuzzModel, int? page)
        {
            int QueryNumber = Int32.Parse(fizzbuzzModel.inputString);
            DayOfWeek CurrentDay = _dayTimeProvider.GetDay();
            int pageNumber = page ?? 1;
            fizzbuzzModel.outputList = (PagedList<string>)_fizzbuzzLogic.GetFizzBuzzList(QueryNumber, CurrentDay).ToPagedList(pageNumber, 10);

            return View(fizzbuzzModel);
        }

        [HttpGet]
        public ActionResult Result(string inputString, int? page)
        {
            if (inputString == null)
            {
                return RedirectToAction("Index", "Home");
            }
            int QueryNumber = Int32.Parse(inputString);
            FizzbuzzModel fizzbuzzModel = new FizzbuzzModel
            {
                inputString = inputString
            };
            DayOfWeek CurrentDay = _dayTimeProvider.GetDay();
            int pageNumber = page ?? 1;
            fizzbuzzModel.outputList = (PagedList<string>)_fizzbuzzLogic.GetFizzBuzzList(QueryNumber, CurrentDay).ToPagedList(pageNumber, 10);

            return View(fizzbuzzModel);
        }
    }
}