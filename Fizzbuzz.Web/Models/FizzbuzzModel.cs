﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PagedList;

namespace Fizzbuzz.Web.Models
{
    public class FizzbuzzModel
    {
        public string inputString { get; set; }
        public PagedList<string> outputList { get; set; }
    }
}